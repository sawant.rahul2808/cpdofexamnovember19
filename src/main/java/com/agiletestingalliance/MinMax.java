package com.agiletestingalliance;

public class MinMax {

    public int getMaxValue(int first, int second) {
        if (second > first){
            return second;
        }
        else {
            return first;
        }
    }

}
