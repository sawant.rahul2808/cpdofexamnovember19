package com.agiletestingalliance;

import org.junit.Assert;
import org.junit.Test;

public class MinMaxTest {

    @Test
    public void testMaxValue(){
        Assert.assertEquals("firstMax",3,new MinMax().getMaxValue(2,3));
    }

    @Test
    public void testMaxValueForZero(){
        Assert.assertEquals("maxWhenZero",0,new MinMax().getMaxValue(0,0));
    }

    @Test
    public void testMaxValueForMinus(){
        Assert.assertEquals("maxForNegative",0,new MinMax().getMaxValue(0,-1));
    }
}
